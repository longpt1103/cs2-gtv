export const getLinkSteamRedirect = (steamConfigData = {}) => {
  let steamRedirectUri = ``
  const {
    openid_op_endpoint,
    openid_claimed_id,
    openid_identity,
    openid_ns,
    openid_mode,
  } = steamConfigData
  if (
    openid_op_endpoint &&
    openid_claimed_id &&
    openid_identity &&
    openid_ns &&
    openid_mode
  ) {
    const queries = [
      { key: 'openid.claimed_id', value: openid_claimed_id },
      { key: 'openid.identity', value: openid_identity },
      { key: 'openid.mode', value: openid_mode },
      { key: 'openid.ns', value: openid_ns },
      { key: 'openid.realm', value: window.location.origin },
      { key: 'openid.return_to', value: window.location.href },
    ]
    let queryString = ''
    queries.forEach((query, index) => {
      queryString += `${query.key}=${query.value}`
      if (index < queries.length - 1) queryString += `&`
    })
    steamRedirectUri = `${openid_op_endpoint}?${queryString}`
  }
  return steamRedirectUri
}
