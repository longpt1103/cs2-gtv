import { generatePath } from 'react-router-dom'
import { routes } from 'route-path'
import history from './history'

export const generateGameModePath = (mode = '') => {
  return generatePath(routes.gamemodeType, { mode })
}

export const pushToGameModePath = (mode = '') => {
  if (history) {
    history.push(generateGameModePath(mode))
  }
}
