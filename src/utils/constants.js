export const TIME_EXPIRE_TOKEN = 0.5 * 60 * 60 * 1000 // 0.5 hours
export const EXPIRE_BEFORE = 10 * 60 * 1000 // expire when `now > expireTokenAt - EXPIRE_BEFORE`
export const STEAM_API_KEY = '8356942B53F59221CA7E1DB40C99E539'

export const FILTER_GAMEMODE = {
  fivevsfive: '5vs5',
  deathmatch: 'deathmatch',
  retake: 'retake',
  bhop: 'bhop',
  arena: 'arena',
  clutch: 'clutch',
  zombieEscape: 'zm',
}
