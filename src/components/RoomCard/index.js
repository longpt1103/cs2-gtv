import { memo } from 'react'
import clsx from 'clsx'
import { pushToGameModePath } from 'utils/route'

const renderStatusText = (type = 'default') => {
  if (type === 'default') return ''
  return type.split('-').join(' ')
}

const RoomCard = ({
  type = 'default',
  src = '',
  players = 0,
  mode = '',
  modePath = '',
  onClick,
}) => {
  const onClickCard = () => {
    pushToGameModePath(modePath)
    onClick?.()
  }
  return (
    <div
      className={clsx('room-card saira-extra-condensed has-overlay', {
        [`room-card room-card--${type}`]: type !== 'default',
      })}
      onClick={onClickCard}
    >
      {type !== 'default' ? (
        <span className="status">{renderStatusText(type).toUpperCase()}</span>
      ) : null}
      <img alt="room" src={src} className="room-img" />
      <div className="mt-auto">
        <div className="label label--mode">{mode}</div>
        <div className="label label--player">
          {!players ? '' : `${players} playing`}
        </div>
      </div>
    </div>
  )
}

export default memo(RoomCard)
