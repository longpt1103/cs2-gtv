import { createSlice } from '@reduxjs/toolkit'
import { fetchServerList } from './asyncThunk'

const initialState = {
  isPending: false,
  serverList: [],
}

export const slice = createSlice({
  name: 'steam',
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(fetchServerList.pending, (state) => {
      state.isPending = true
      state.serverList = []
    })
    builder.addCase(fetchServerList.fulfilled, (state, { payload }) => {
      const { response } = payload
      console.log('fetchServerList.fulfilled: ', { response })
      const servers = response?.servers || []
      state.serverList = servers
      state.isPending = false
    })
    builder.addCase(fetchServerList.rejected, (state) => {
      state.isPending = false
    })
  },
})

export const { actions: steamActions, reducer: steamReducer } = slice
export { initialState }
