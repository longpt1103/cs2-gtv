import { createAsyncThunk } from '@reduxjs/toolkit'
import { getServerList } from 'services/steam'

const fetchServerList = createAsyncThunk(
  'steam/fetchServerList',
  async (params = {}) => {
    const response = await getServerList(params)
    return response
  },
)

export { fetchServerList }
