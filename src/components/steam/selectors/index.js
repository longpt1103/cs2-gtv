import { createSelector } from '@reduxjs/toolkit'

import { initialState } from '../slices'

const selectSlice = (state) => state.steam || initialState

export const selectServerList = createSelector(
  [selectSlice],
  (state) => state.serverList,
)

export const selectIsPending = createSelector(
  [selectSlice],
  (state) => state.isPending,
)
