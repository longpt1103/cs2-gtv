import { useEffect } from 'react'
import { useParams } from 'react-router-dom'
import { useDispatch } from 'react-redux'
import { fetchServerList } from 'components/steam/slices/asyncThunk'
import { FILTER_GAMEMODE } from 'utils/constants'

const filterGamemode = Object.values(FILTER_GAMEMODE)

const Watcher = () => {
  const dispatch = useDispatch()
  const params = useParams()
  useEffect(() => {
    const value = params?.mode
    if (filterGamemode.includes(value)) {
      const filters = [{ key: 'gametype', value }]
      dispatch(
        fetchServerList({
          filters,
        }),
      )
    }
  }, [params?.mode])
  return null
}

export default Watcher
