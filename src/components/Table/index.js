import { memo } from 'react'
import { useSelector } from 'react-redux'
import { selectIsPending } from 'components/steam/selectors'
import TableHeader from './TableHeader'
import Body from './Body'
import Watcher from './Watcher'
import Loading from './Loading'

const Table = () => {
  const isLoading = useSelector(selectIsPending)
  return (
    <>
      <Watcher />
      {isLoading ? (
        <Loading />
      ) : (
        <table className="table">
          <TableHeader />
          <Body />
        </table>
      )}
    </>
  )
}

export default memo(Table)
