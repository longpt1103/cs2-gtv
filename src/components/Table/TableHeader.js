const TableHeader = () => {
  return (
    <thead>
      <tr>
        <th>#</th>
        <th>Server</th>
        <th>IP</th>
        <th>Port</th>
        <th>Ping</th>
        <th>Player</th>
        <th>Map</th>
        <th>Connect</th>
      </tr>
    </thead>
  )
}

export default TableHeader
