import { memo } from 'react'
import { useSelector, useDispatch } from 'react-redux'
import modal from 'components/modal/provider'
import { selectIsSignIn, selectSteamId } from 'components/auth/selectors'
import { selectServerList } from 'components/steam/selectors'
import { fetchSteamConfig } from 'components/auth/slices/asyncThunk'
import { ReactComponent as PlayIcon } from 'assets/icons/play.svg'
import { ReactComponent as PlayDarkIcon } from 'assets/icons/play-dark.svg'
import EmptyRow from './EmptyRow'

const defaultItem = {}

const ButtonConnect = ({ addr = '' }) => {
  const dispatch = useDispatch()
  const isSignIn = useSelector(selectIsSignIn)
  const steamId = useSelector(selectSteamId)
  const hasConnect = !!(isSignIn && steamId)
  const onClickConnect = () => {
    if (!isSignIn) {
      modal.common.loginRequired()
      return
    }
    // signined
    if (steamId) {
      const connect = `steam://connect/${addr}`
      alert(connect)
    } else {
      // connect steam first
      dispatch(fetchSteamConfig())
    }
  }
  return (
    <button
      className={`btn btn-wide ${
        hasConnect ? 'btn-game-blue' : 'btn-outline-default'
      }`}
      onClick={onClickConnect}
    >
      {hasConnect ? <PlayIcon /> : <PlayDarkIcon />}
    </button>
  )
}

const Item = memo(({ item = defaultItem, index }) => {
  const {
    name = '',
    addr = '',
    ping,
    players = 0,
    max_players = 0,
    map = '',
    gameport = 0,
  } = item
  const [ip, port] = addr.split(':')
  return (
    <>
      <EmptyRow />
      <tr>
        <td>{`#${index + 1}`}</td>
        <td>{name}</td>
        <td>{ip}</td>
        <td>{gameport || port}</td>
        <td
          className={`text-game-${ping === 20 ? 'green' : 'yellow'}`}
        >{`${ping}ms`}</td>
        <td>{`${players}/${max_players}`}</td>
        <td>{map}</td>
        <td>
          <ButtonConnect addr={addr} />
        </td>
      </tr>
    </>
  )
})

const Body = () => {
  const servers = useSelector(selectServerList)
  return (
    <tbody>
      {servers.map((item, index) => {
        const key = `${item.steamid}-${item.addr}`
        return <Item key={key} item={item} index={index} />
      })}
    </tbody>
  )
}

export default Body
