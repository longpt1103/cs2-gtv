import axiosService from 'services/axios'
import baseUrl from 'services/baseUrl'

export const getServerList = ({ filters = [], limit = 30, ...rest }) => {
  const s = '\\' // \ char
  const filterString = filters.reduce((line, filter) => {
    line += `${s}${filter.key}${s}${filter.value}`
    return line
  }, '')
  const url = baseUrl.steamIGameServersService({
    path: 'GetServerList',
    query: new URLSearchParams({
      limit,
      ...(filterString ? { filter: filterString } : {}),
      ...rest,
    }).toString(),
  })
  return axiosService.get(url)
}
