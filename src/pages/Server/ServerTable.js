import React from 'react'
import Table from 'components/Table'
import BtnGroupGameMode from './BtnGroupGameMode'

const ServerTable = () => {
  return (
    <div className="main-container">
      <BtnGroupGameMode />
      <div className="table-wrapper">
        <Table />
      </div>
    </div>
  )
}

export default ServerTable
